const supertest = require('supertest')
const mongoose = require('mongoose')
// const fs = require('fs')
require('dotenv').config()
process.log = {}

const User = require('../models/user')
// const Review = require('../models/review')

const { generateUser } = require('./fixtures/user')
const app = require('../app')
const request = supertest(app)

const user = generateUser()

async function removeAllCollections () {
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    await collection.deleteMany()
  }
}

async function dropAllCollections () {
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    try {
      await collection.drop()
    } catch (error) {
      // Sometimes this error happens, but you can safely ignore it
      if (error.message === 'ns not found') return
      // This error occurs when you use it.todo. You can
      // safely ignore this error too
      if (error.message.includes('a background operation is currently running')) return
    }
  }
}

beforeAll(async (done) => {
  mongoose
    .connect(process.env.DB_CONNECTION_TEST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => {
      console.log('connected')
    })
    .catch(err => console.error(err))
  await removeAllCollections()
  done()
})

// Disconnect Mongoose
afterAll(async (done) => {
  await dropAllCollections()
  await mongoose.connection.close()
  done()
})

describe('User endpoint', () => {
  it('Create a new user', async done => {
    try {
      const res = await request.post('/api/v1/users')
        .send(user)

      const { status, data } = res.body

      expect(status).toBe(true)
      expect(res.statusCode).toEqual(201)
      expect(typeof data).toEqual('object')
      expect(data).toHaveProperty('token')
      expect(true).toEqual(true)
      done()
    } catch (err) {
      console.log(err)
    }
  })

  it('Should activate the corresponding user', done => {
    User.authenticate({ email: user.email, password: user.password })
      .then(user => {
        request
          .get(`/api/v1/users/activate?token=${user.token}`)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(typeof data).toEqual('object')
            done()
          })
      })
  })

  it('Login the created user', done => {
    request.post('/api/v1/users/login')
      .set('Content-Type', 'application/json')
      .send({ email: user.email, password: user.password })
      .then(res => {
        const { status, data } = res.body

        expect(status).toBe(true)
        expect(res.statusCode).toEqual(200)
        expect(typeof data).toEqual('object')
        expect(data).toHaveProperty('token')
        expect(true).toEqual(true)
        done()
      })
  })
})

describe('Movie Endpoint', () => {
  it('Create a New Movie', async done => {
    const movie = {
      title: "movie's title",
      synopsis: "movie's synopsis",
      trailer: "movie's youtube link",
      casts: "movie's main casts",
      genres: 'Action',
      details: 'release year, director, etc'
    }
    User.authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.post('/api/v1/movies')
          .set('Content-Type', 'application/json')
          .set('Authorization', 'Bearer ' + data.token)
          .send(movie)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(typeof data).toEqual('object')
            done()
          })
      })
  })

  it('Show all Movies', async done => {
    request.get('/api/v1/movies')
      .then(res => {
        const { status, data } = res.body
        expect(status).toBe(true)
        expect(typeof data).toBe('object')
        done()
      })
  })

  it('Show all Genres of The Movies', async done => {
    request.get('/api/v1/movies/genres')
      .then(res => {
        const { status, data } = res.body
        expect(status).toBe(true)
        expect(typeof data).toBe('object')
        done()
      })
  })

  it('Show The Info of The Selected Movie Chosen By Movie Id', async done => {
    request.get('/api/v1/movies')
      .then(res => {
        request.get(`/api/v1/movies/${res.body.data.docs[0]._id}`)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(typeof data).toBe('object')
            done()
          })
      })
  })

  it('Should Update The Selected Movie Chosen By Movie Id', async done => {
    const updateMovie = {
      title: "update movie's title",
      synopsis: "update movie's synopsis",
      trailer: "update movie's youtube link",
      casts: "update movie's main casts",
      genres: 'Action',
      details: 'release year, director, etc'
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(dt => {
        request.get('/api/v1/movies')
          .then(result => {
            request.put(`/api/v1/movies/${result.body.data.docs[0]._id}`)
              .set('Content-Type', 'application/json')
              .set('Authorization', 'Bearer ' + dt.token)
              .send(updateMovie)
              .then(res => {
                expect(res.body.status).toBe(true)
                // expect(typeof responseData).toBe('object')
                done()
              })
          })
      })
  })

  it('Should Delete The Selected Movie Chosen By Movie Id', async done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(dt => {
        request.get('/api/v1/movies')
          .then(result => {
            request.delete(`/api/v1/movies/${result.body.data.docs[0]._id}`)
              .set('Content-Type', 'application/json')
              .set('Authorization', 'Bearer ' + dt.token)
              .then(res => {
                expect(res.body.status).toBe(true)
                // expect(typeof responseData).toBe('object')
                done()
              })
          })
      })
  })
})
