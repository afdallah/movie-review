const faker = require('faker')

const password = faker.internet.password()
exports.generateUser = function () {
  return {
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
    email: faker.internet.email(),
    password: password,
    password_confirmation: password
  }
}
