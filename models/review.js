const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
mongoosePaginate.paginate.options = {
  limit: 10,
  sort: { createdAt: -1 }
}
const Schema = mongoose.Schema

const reviewSchema = new Schema({
  rating: {
    type: Number,
    required: true,
    max: 10,
    min: 1,
    default: 0
  },
  title: {
    type: 'string',
    default: false
  },
  content: {
    type: 'string',
    required: true
  },
  movie_id: {
    type: Schema.Types.ObjectId,
    ref: 'Movie'
  },
  reviewer: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  versionKey: false,
  timestamps: true
})

reviewSchema.plugin(mongoosePaginate)
const Review = mongoose.model('Review', reviewSchema)

module.exports = Review
