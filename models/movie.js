const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const Schema = mongoose.Schema
const genreList = require('../helpers').getGenres()

mongoosePaginate.paginate.options = {
  limit: 10,
  sort: { createdAt: -1 }
}

const movieSchema = Schema(
  {
    user_id: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    title: {
      type: String,
      required: 'You must supply title',
      trim: true
    },
    reviews: [{ type: Schema.Types.ObjectId, ref: 'Review' }],
    synopsis: {
      type: String,
      trim: true
    },
    trailer: {
      type: String
    },
    rating: {
      type: Number,
      default: 0
    },
    poster: {
      type: String
    },
    casts: [{ type: Object }],
    genres: [
      {
        type: String,
        enum: genreList
      }
    ],
    movie_details: { type: Object }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

movieSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Movie', movieSchema)
