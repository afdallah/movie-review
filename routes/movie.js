const express = require('express')
const router = express.Router()

const movieController = require('../controllers/movie')
const isAuthenticated = require('../middlewares/authenticate')

// Create movie
router.post('/', isAuthenticated, movieController.create)
router.get('/', movieController.getAll)
router.get('/genres', movieController.getGenres)
router.get('/:_id', movieController.getById)
router.put('/:_id', isAuthenticated, movieController.updateById)
router.delete('/:_id', isAuthenticated, movieController.deleteById)

module.exports = router
