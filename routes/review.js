const express = require('express')
const router = express.Router()

const isAuthenticated = require('../middlewares/authenticate')
const review = require('../controllers/review')

router.post('/:movie_id/create/', isAuthenticated, review.create)
router.get('/show', review.show)
router.put('/update/:review_id', isAuthenticated, review.update)
router.delete('/delete/:review_id', isAuthenticated, review.remove)

module.exports = router
