const path = require('path')
module.exports = {
  collectCoverage: false,
  collectCoverageFrom: ['./controllers/**/*.js', './models/**/*.js', './routes/**/*.js', '!routes/index.js', '!controllers/auth.js'],
  coverageDirectory: './public/coverage',
  testTimeout: 10000,
  testEnvironment: 'node',
  modulePathIgnorePatterns: [
    '__tests__/fixtures',
    '__tests__/CustomSequencer.js'
  ],
  coveragePathIgnorePatterns: [
    '/node_modules/'
  ],
  coverageReporters: ['lcov']
  // testSequencer: path.join(__dirname, '__tests__', 'CustomSequencer.js')
}
