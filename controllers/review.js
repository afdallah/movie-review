const Review = require('../models/review')
const Movie = require('../models/movie')
const { success, error } = require('../helpers/handler')

const show = async (req, res, next) => {
  const { page = 1, limit = 15 } = req.query

  try {
    const reviews = await Review.paginate({}, {
      limit,
      page,
      select: '-movie_details',
      populate: {
        path: 'reviewer',
        select: '-encrypted_password'
      }
    })
    success(res, 200, reviews)
  } catch (err) {
    next(err)
  }
}

const create = async (req, res, next) => {
  const { rating, title, content } = req.body
  const { movie_id } = req.params
  const reviewer = req.user._id

  try {
    // Validate: max one review per user
    const savedReview = await Review.findOne({ movie_id })
    // current user exists in review, then stop the process and throw error
    console.log(savedReview && savedReview.reviewer.toString() === req.user._id.toString())
    if (savedReview && savedReview.reviewer.toString() === req.user._id.toString()) return error(400, "You can't add any more review")

    const movie = await Movie.findById(movie_id)
    const review = await new Review({
      reviewer,
      movie_id,
      rating,
      title,
      content
    })
    await review.save()

    const reviews = await Review.find()

    // calculate overall rating
    const ratings = reviews
      .map(r => r.rating)
      .reduce((acc, curr) => acc + curr)

    movie.rating = (ratings / reviews.length).toFixed(1)
    movie.reviews.push(review._id)
    await movie.save()

    success(res, 200, review)
  } catch (err) {
    next(err)
  }
}

const update = async (req, res, next) => {
  try {
    const updated = await Review.findOneAndUpdate(
      { reviewer: req.user._id, _id: req.params.review_id },
      req.body,
      { new: true }
    )
    // if (!updated) return error('400', 'You are not the owner of this review')

    const movie = await Movie.findById(updated.movie_id).populate('reviews')

    const updt = movie.reviews.splice(movie.reviews.indexOf(updated), 1, updated)
    movie.reviews = updt

    // calculate overall rating
    const ratings = movie.reviews
      .map(r => r.rating)
      .reduce((acc, curr) => acc + curr)

    movie.rating = (ratings / movie.reviews.length).toFixed(1)
    await movie.save()

    success(res, 200, updated)
  } catch (err) {
    next(err)
  }
}

const remove = async (req, res, next) => {
  try {
    const removed = await Review.findOneAndDelete({
      reviewer: req.user._id,
      _id: req.params.review_id
    })

    if (!removed) return error('400', 'You are not the owner of this review')
    const movie = await Movie.findById(removed.movie_id)
    const filtered = await movie.reviews.filter(r => r._id.toString() !== removed._id.toString())
    movie.reviews = filtered
    await movie.save()

    success(res, 200, removed)
  } catch (err) {
    next(err)
  }
}

module.exports = {
  create,
  update,
  remove,
  show
}
