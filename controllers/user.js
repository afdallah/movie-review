const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const ImageKit = require('imagekit')
const fs = require('fs')

const User = require('../models/user')
const Review = require('../models/review')
const mailer = require('../helpers/mailer')

let registerHTML = fs
  .readFileSync(`${__dirname}/../mail-templates/activate-account.html`)
  .toString()

const imagekit = new ImageKit({
  publicKey: process.env.IMAGEKIT_PUBLIC_KEY,
  privateKey: process.env.IMAGEKIT_PRIVATE_KEY,
  urlEndpoint: process.env.IMAGEKIT_ENDPOINT
})

function isValidId (id) {
  const regex = /^[a-fA-F0-9]{24}$/

  return regex.test(id)
}

const { success, error } = require('../helpers/handler')

// Create / Register user
exports.create = async (req, res, next) => {
  const { first_name, last_name, email, password, password_confirmation } = req.body

  try {
    if (!password) return error(422, 'Password is required')
    // Check if password and its confirmation are the same
    if (password !== password_confirmation) return error(400, 'Password and its confirmation must be the same')

    const salt = await bcrypt.genSalt(10)
    const encrypted_password = await bcrypt.hash(password, salt)

    const users = await User.find()
    let user
    if (users.length < 1) {
      user = await User.create({ first_name, last_name, email, encrypted_password, role: 'admin' })
    } else {
      user = await User.create({ first_name, last_name, email, encrypted_password })
    }

    const token = jwt.sign({ _id: user._id, role: user.role }, process.env.JWT_SECRET_KEY, { expiresIn: '1d' })

    registerHTML = registerHTML.replace('{{ name }}', first_name)
    registerHTML = registerHTML.replace(
      '{{ activate_account_url }}',
      `${process.env.BASE_URL}/activate?token=${token}`
    )

    if (!process.env.NODE_ENV === 'test') {
      mailer({
        from: 'noreply@sukatodo.com',
        // to: user.email,
        text: 'Sukatodo',
        html: registerHTML
      })
    }

    success(res, 201, { ...user._doc, token, encrypted_password: undefined })
  } catch (err) {
    next(err)
  }
}

exports.getAll = async (req, res, next) => {
  try {
    if (req.user.role !== 'admin') return error(401, 'You are not authorized to do this action')
    const users = await User.find().select('-encrypted_password')

    success(res, 200, users)
  } catch (err) {
    next(err)
  }
}

exports.getById = async (req, res, next) => {
  try {
    if (!req.params.id || !isValidId(req.params.id)) return error(422, 'Invalid _id supplied')

    const reviews = await Review.find({ reviewer: req.params.id })
    const user = await User.findById(req.params.id).select(
      '-encrypted_password'
    )

    success(res, 200, { ...user._doc, reviews })
  } catch (err) {
    next(err)
  }
}

exports.getMyProfile = async (req, res, next) => {
  const user = await User.findById(req.user._id).select(
    '-encrypted_password'
  )

  success(res, 200, user)
}

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body
    const user = await User.authenticate({ email, password })

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

// Update
exports.update = async (req, res, next) => {
  delete req.body.is_confirmed

  try {
    let user
    if (req.file) {
      const uploaded = await imagekit.upload({
        file: req.file.buffer.toString('base64'),
        fileName: `${req.file.fieldname}-${
          req.file.originalname.split('.')[0]
        }-${Date.now()}.${req.file.mimetype.split('/')[1]}`
      })

      user = await User.findByIdAndUpdate(
        req.user._id,
        {
          image: uploaded.url
        },
        { new: true }
      ).select('-encrypted_password')
    } else {
      user = await User.findByIdAndUpdate(
        req.user._id,
        {
          $set: { ...req.body }
        },
        { new: true }
      ).select('-encrypted_password')
    }

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.activate = async (req, res, next) => {
  const { token } = req.query

  try {
    const validUser = jwt.verify(token, process.env.JWT_SECRET_KEY)
    if (!validUser._id) return error(400, 'Token is invalid')

    const user = await User
      .findByIdAndUpdate(validUser._id, { $set: { is_confirmed: true } }, { new: true })

    if (!user) return error(404, 'User not found')

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.delete = async (req, res, next) => {
  try {
    if (!req.params.id || !isValidId(req.params.id)) return error(400, 'Invalid user id')

    const user = await User.findByIdAndUpdate(req.params.id).select('-encrypted_password')
    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.deleteMe = async (req, res, next) => {
  const user = await User.findByIdAndDelete(req.user._id).select('-encrypted_password')
  success(res, 200, user)
}
