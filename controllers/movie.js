const Movie = require('../models/movie.js')
const { success, error } = require('../helpers/handler')

exports.create = (req, res, next) => {
  if (req.user.role !== 'admin') return error(401, 'You are not authorized to do this action')

  const {
    title,
    trailer,
    synopsis,
    genres,
    casts,
    poster,
    movie_details
  } = req.body

  Movie.create({
    user_id: req.user._id,
    title,
    trailer,
    poster: poster,
    synopsis,
    genres,
    casts,
    movie_details
  })
    .then(movie => {
      return success(res, 200, movie)
    })
    .catch(next)
}

exports.getAll = async (req, res, next) => {
  let { search = '', genre = null, page = 1, limit = 15 } = req.query
  genre = genre ? genre.charAt(0).toUpperCase() + genre.slice(1) : genre

  let query = {}

  if (search) {
    query = {
      title: { $regex: search, $options: 'i' }
    }
  }

  if (genre) {
    query = {
      genres: genre
    }
  }

  if (search && genre) {
    query = {
      title: { $regex: search, $options: 'i' },
      genres: genre
    }
  }

  try {
    const movies = await Movie.paginate(query, { limit, page, select: '-movie_details' })
    success(res, 200, movies)
  } catch (err) {
    next(err)
  }
}

exports.getById = async (req, res, next) => {
  try {
    const movie = await Movie.findById(req.params._id).populate('reviews', 'reviewer')

    success(res, 200, movie)
  } catch (err) {
    next(err)
  }
}

exports.getGenres = async (req, res, next) => {
  try {
    const movies = await Movie.find()

    const genres = []

    movies.forEach(movie => movie.genres.forEach(genre => {
      if (!genres.includes(genre)) genres.push(genre)
    }))

    success(res, 200, genres)
  } catch (err) {
    next(err)
  }
}

exports.updateById = async (req, res, next) => {
  try {
    if (req.user.role !== 'admin') return error(401, 'You are not authorized to do this action')
    const updated = await Movie.findByIdAndUpdate(req.params._id, req.body, { new: true })
    success(res, 200, updated)
  } catch (err) {
    next(err)
  }
}

exports.deleteById = async (req, res, next) => {
  try {
    if (req.user.role !== 'admin') return error(401, 'You are not authorized to do this action')
    const deleted = await Movie.findByIdAndRemove(req.params._id)
    success(res, 200, deleted)
  } catch (err) {
    next(err)
  }
}
