const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
const cors = require('cors')
const swaggerUi = require('swagger-ui-express')
const app = express()
process.log = {}

const env = process.env.NODE_ENV || 'development'
const connectionString = {
  development: process.env.DB_CONNECTION,
  test: process.env.DB_CONNECTION_TEST,
  staging: process.env.DB_CONNECTION,
  production: process.env.DB_CONNECTION
}

mongoose.connect(
  connectionString[env],
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  }
)
  .then(() => {
    if (process.env.NODE_ENV !== 'test') console.log('Database connected!')
  })
  .catch(err => console.log(err))

mongoose.set('useFindAndModify', false)

const swaggerDocument = require('./swagger.json')

// routers
const viewRouter = require('./routes')
const userRouter = require('./routes/user')
const movieRouter = require('./routes/movie')
const reviewRouter = require('./routes/review')

// controllers
const userController = require('./controllers/user')

// middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static('public'))
app.set('view engine', 'pug')
// app.use(morgan('dev'))
app.use(cors())

app.use(
  '/documentation',
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument, {
    customCss:
      '.topbar-wrapper { justify-content: center; text-align: center } .swagger-ui .topbar a { justify-content: center } .swagger-ui.swagger-container{ max-width: 100% } .swagger-ui { margin:auto; max-width: 70%;  } .wrapper .block { margin: 0 -20px }'
  })
)

// routers
app.use(viewRouter)
app.use('/api/v1/users', userRouter)
app.use('/api/v1/movies', movieRouter)
app.use('/api/v1/reviews', reviewRouter)

// Activate account
app.get('/activate', userController.activate)

app.use(function (req, res, next) {
  res.status(404).send('The page you are looking for is not found')
})

// Error handler
app.use(function (err, req, res, next) {
  const { statusCode = 400, message } = err

  res.status(statusCode).json({
    status: false,
    statusCode,
    message
  })
})

module.exports = app
