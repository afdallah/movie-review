const nodemailer = require('nodemailer')
const sgTransport = require('nodemailer-sendgrid-transport')

const options = {
  auth: {
    api_key: process.env.SENDGRID_API_KEY
  }
}

const client = nodemailer.createTransport(sgTransport(options))

async function mailer (email) {
  var defaultEmail = {
    from: 'dalh@gmail.com',
    to: 'afdallah.war@gmail.com',
    subject: 'Test sukatodo',
    text: 'Welcome to sukatodo',
    html: 'Welcome to <b>sukatodo</b>'
  }

  try {
    await client.sendMail(Object.assign(defaultEmail, email))
  } catch (err) {
    console.log(err)
  }
}

module.exports = mailer
