{
  "swagger": "2.0",
  "info": {
    "description": "A minimalistic movie review application",
    "version": "1.0.0",
    "title": "Notflixtv",
    "contact": {
      "email": "hello@notflixtv.com"
    }
  },
  "host": "",
  "basePath": "/api/v1",
  "tags": [
    {
      "name": "User Endpoints",
      "description": "All endpoints related to users"
    },
    {
      "name": "Movie Endpoints",
      "description": "all endpoints related to movies"
    },
    {
      "name": "Review Endpoints",
      "description": "all endpoints related to reviews"
    }
  ],
  "paths": {
    "/users": {
      "post": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Create user",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Created user object",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "first_name": {
                  "type": "string",
                  "example": "John"
                },
                "last_name": {
                  "type": "string",
                  "example": "Doe"
                },
                "email": {
                  "type": "string",
                  "example": "john@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "pass"
                },
                "password_confirmation": {
                  "type": "string",
                  "example": "pass"
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserWithToken"
            }
          },
          "400": {
            "description": "Password and its confirmation must be the same"
          }
        }
      },
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get all users",
        "description": "",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Array of user object"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      },
      "put": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Update current user",
        "consumes": [
          "multipart/form-data",
          "application/json"
        ],
        "description": "This can only be done by the logged in user.",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "authorization": []
          }
        ],
        "parameters": [
          {
            "in": "formData",
            "name": "image",
            "type": "file",
            "description": "The file to upload."
          },
          {
            "in": "formData",
            "name": "first_name",
            "type": "string",
            "required": false,
            "description": "First name of the user"
          },
          {
            "in": "formData",
            "name": "last_name",
            "type": "string",
            "required": false,
            "description": "Last name of the user"
          },
          {
            "in": "formData",
            "name": "email",
            "type": "string",
            "required": false,
            "description": "Email of the user."
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update user"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/activate": {
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get current user details",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "token",
            "type": "string",
            "description": "Token to be use to activate the user account"
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "400": {
            "description": "Token is invalid"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/login": {
      "post": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Logs user into the system",
        "description": "Notice that `full_name` property is not present in the response, but actually you can use the value by calling property full_name directly. example: `user.full_name`. Read more about mongoose virtuals https://mongoosejs.com/docs/tutorials/virtuals.html",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Created user object",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "email": {
                  "type": "string",
                  "example": "john@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "pass"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserWithToken"
            }
          },
          "404": {
            "description": "User not found"
          },
          "422": {
            "description": "You must supply an email and a password"
          }
        }
      }
    },
    "/users/me": {
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get current user details",
        "description": "",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "authorization": []
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "404": {
            "description": "User not found"
          },
          "422": {
            "description": "Invalid _id supplied"
          }
        }
      },
      "delete": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Delete current user",
        "description": "",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "authorization": []
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "400": {
            "description": "Invalid _id supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/{_id}": {
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get user by object id",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "_id",
            "in": "path",
            "description": "Object id of the user",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "400": {
            "description": "Invalid _id supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "delete": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Delete user",
        "description": "This can only be done by the logged in user.",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "_id",
            "in": "path",
            "description": "The name that needs to be deleted",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Succesfully delete user"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/movies": {
      "post": {
        "tags": [
          "Movie Endpoints"
        ],
        "summary": "Create new movie details",
        "description": "Only super admin can do this action",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "$ref": "#/definitions/CreateMovieRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully create review"
          },
          "400": {
            "description": "Something went wrong"
          },
          "401": {
            "description": "You are not authorized to do this action"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      },
      "get": {
        "tags": [
          "Movie Endpoints"
        ],
        "summary": "Get all movies",
        "description": "Get all movies, you can also filter by its genre",
        "parameters": [
          {
            "name": "page",
            "in": "query",
            "description": "Page number",
            "type": "number",
            "items": {
              "default": 1
            }
          },
          {
            "name": "limit",
            "in": "query",
            "description": "limit of item to show",
            "type": "number",
            "items": {
              "default": 10
            }
          },
          {
            "name": "search",
            "in": "query",
            "description": "Search using partial string",
            "type": "string"
          },
          {
            "name": "genre",
            "in": "query",
            "type": "array",
            "items": {
              "type": "string",
              "enum": [
                "Action",
                "Adventure",
                "Animation",
                "Comedy",
                "Crime",
                "Documentary",
                "Drama",
                "Family",
                "Fantasy",
                "History",
                "Horror",
                "Music",
                "Mystery",
                "Romance",
                "Science Fiction",
                "Tv Movie",
                "Thriller",
                "War",
                "Western"
              ]
            },
            "collectionFormat": "multi"
          }
        ],
        "responses": {
          "200": {
            "description": "Should return array of Movies"
          }
        }
      }
    },
    "/movies/genres": {
      "get": {
        "tags": [
          "Movie Endpoints"
        ],
        "summary": "Get all genres",
        "description": "Get all genres based on the created movies",
        "responses": {
          "200": {
            "description": "Should return array of Movies"
          }
        }
      }
    },
    "/movies/{movie_id}": {
      "get": {
        "tags": [
          "Movie Endpoints"
        ],
        "summary": "Show the info of the chosen movie by movieId",
        "description": "Get movie details",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "movie_id",
            "in": "path",
            "description": "fill the chosen movie id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully show movie info"
          },
          "422": {
            "description": "Your input might be wrong"
          }
        }
      },
      "put": {
        "tags": [
          "Movie Endpoints"
        ],
        "summary": "update movie by movieId",
        "description": "Ask superuser for update movie",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "movie_id",
            "in": "path",
            "description": "fill the chosen movie id",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "body",
            "schema": {
              "$ref": "#/definitions/CreateMovieRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update movie"
          },
          "404": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      },
      "delete": {
        "tags": [
          "Movie Endpoints"
        ],
        "summary": "Delete movie",
        "description": "You can get movie id from /movies endpoint",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "movie_id",
            "in": "path",
            "description": "fill the chosen movie id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully delete movie"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/reviews/{movie_id}/create": {
      "post": {
        "tags": [
          "Review Endpoints"
        ],
        "summary": "create new review for the selected movie",
        "description": "Ask user for review's rating, review's title, and review's content",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "movie_id",
            "in": "path",
            "description": "fill the chosen movie id",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CreateReviewRequest"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Successfully create review"
          },
          "400": {
            "description": "You can't add any more review"
          },
          "422": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/reviews/show": {
      "get": {
        "tags": [
          "Review Endpoints"
        ],
        "summary": "Get user's all review",
        "description": "Showing reviews by asking user for page number (only show max 10 tasks per page)",
        "parameters": [
          {
            "name": "page",
            "in": "query",
            "description": "page number",
            "type": "number",
            "items": {
              "default": 1
            }
          },
          {
            "name": "limit",
            "in": "query",
            "description": "limit of the result",
            "type": "number",
            "items": {
              "default": 1
            }
          }
        ],
        "consumes": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Should return array of Reviews"
          }
        }
      }
    },
    "/reviews/update/{review_id}": {
      "put": {
        "tags": [
          "Review Endpoints"
        ],
        "summary": "update review by reviewId",
        "description": "Ask user for review's rating, review's title, and/or review's content",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "review_id",
            "in": "path",
            "description": "fill the chosen review id",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "body",
            "schema": {
              "$ref": "#/definitions/UpdateReviewRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update review"
          },
          "404": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/reviews/delete/{review_id}": {
      "delete": {
        "tags": [
          "Review Endpoints"
        ],
        "summary": "Delete review by reviewId",
        "consumes": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "review_id",
            "in": "path",
            "description": "fill the chosen review id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Should delete the review"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "authorization": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  },
  "definitions": {
    "User": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "dal"
        },
        "email": {
          "type": "string",
          "example": "dal@gmail.com"
        },
        "image": {
          "type": "string",
          "example": "http://domain.com/image.jpg"
        },
        "encrypted_password": {
          "type": "string",
          "example": "passwordsusah123"
        },
        "is_confirmed": {
          "type": "boolean",
          "example": true
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "UserNoPassword": {
      "type": "object",
      "properties": {
        "first_name": {
          "type": "string",
          "example": "John"
        },
        "last_name": {
          "type": "string",
          "example": "doe"
        },
        "full_name": {
          "type": "string",
          "example": "John doe"
        },
        "email": {
          "type": "string",
          "example": "john@gmail.com"
        },
        "image": {
          "type": "string",
          "example": "http://domain.com/image.jpg"
        },
        "is_confirmed": {
          "type": "boolean",
          "example": true
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "UserWithToken": {
      "type": "object",
      "properties": {
        "first_name": {
          "type": "string",
          "example": "John"
        },
        "last_name": {
          "type": "string",
          "example": "doe"
        },
        "full_name": {
          "type": "string",
          "example": "John doe"
        },
        "email": {
          "type": "string",
          "example": "john@gmail.com"
        },
        "image": {
          "type": "string",
          "example": "http://domain.com/image.jpg"
        },
        "token": {
          "type": "string",
          "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTRlNjgzOWUzZjJhNmY4NTk5YTZhZTciLCJpYXQiOjE1ODIxOTY3OTN9.M1g9J-OnZ0t83xiIvlaDFYX--JwA5IFWQlNzk5wzjIY"
        },
        "is_confirmed": {
          "type": "boolean",
          "example": true
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "CreateMovieRequest": {
      "type": "object",
      "required": [
        "title"
      ],
      "properties": {
        "title": {
          "type": "string",
          "example": "movie's title"
        },
        "reviews": {
          "type": "string",
          "example": "all reviews based on movie id"
        },
        "synopsis": {
          "type": "string",
          "example": "movie's synopsis"
        },
        "trailer": {
          "type": "string",
          "example": "movie's youtube link"
        },
        "casts": {
          "type": "string",
          "example": "movie's main casts"
        },
        "details": {
          "type": "string",
          "example": "release year, director, etc"
        }
      }
    },
    "CreateReviewRequest": {
      "type": "object",
      "required": [
        "rating",
        "title",
        "content"
      ],
      "properties": {
        "rating": {
          "type": "number",
          "example": 3
        },
        "title": {
          "type": "string",
          "example": "review's title"
        },
        "content": {
          "type": "string",
          "example": "review's content"
        }
      }
    },
    "UpdateReviewRequest": {
      "type": "object",
      "properties": {
        "rating": {
          "type": "number",
          "example": "review's rating"
        },
        "title": {
          "type": "string",
          "example": "reviews's title"
        },
        "content": {
          "type": "string",
          "example": "review's content"
        }
      }
    },
    "ApiResponse": {
      "type": "object",
      "properties": {
        "status": {
          "type": "string"
        },
        "data": {
          "type": "string"
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Notflixtodo",
    "url": "https://notflixtv.herokuapp.com"
  }
}
